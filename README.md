 The Bowling Game Task
 
 "Write a program that simulates a Bowling game. Given 2 players, randomly assign roll attempts,
 then call the score for each round together with the total score. Each roll can knock 0-10 pins,
 so make sure spares and strikes are calculated properly. If you’re not familiar with the rules of the game,
 simply google it and you will find what you need."
