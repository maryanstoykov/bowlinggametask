﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BowlingGame
{
    public class Game
    {

        List<Player> Players;
        Random rnd = new Random();
 
        public Game()
        {
            Players = new List<Player>();
            Players.Add(new Player("Player 1"));
            Players.Add(new Player("Player 2"));
        }

        public Game(List<Player> p)
        {
            Players = p;
        }

        public void Start()
        {
            for (int frame = 0; frame < 10; frame++)
            {
                foreach (Player p in Players)
                {
                    PlayFrame(p, frame);
                    p.FramesSet.CalculateFrameScore(frame);
                }
               
                Display(frame);
                
                if (frame < 9)
                {
                    Console.Write("Press any key for the next round...");
                    Console.ReadKey();
                }
                
            }
        }

        void Display(int frame)
        {
            Console.WriteLine("######################################### Frame {0} results #########################################", frame+1);
         
            foreach (Player p in Players)
            {
                Console.ForegroundColor = ConsoleColor.Green;
                Console.WriteLine("*** {0} ***",p.Name);
                Console.ForegroundColor = ConsoleColor.Green;
                Console.WriteLine(p.FramesSet.ToString());
                Console.ResetColor();
            }     
        }

        public void PlayFrame(Player p, int frame)
        {
            int pinsdown = Roll(10);

            p.FramesSet.AddAttempt(frame, pinsdown);//first attempt

            if (pinsdown < 10)// not a strike
            {
                pinsdown = Roll(10-pinsdown);
                p.FramesSet.AddAttempt(frame, pinsdown); // second attempt, frame <10 
            }

            if ((frame == 9) && (pinsdown == 10)&&(p.FramesSet.Frames[9].Attempts.Count==1))// frame 10 and first attempt strike
            {
               // ResetPins();

                pinsdown = Roll(10);// second attempt

                p.FramesSet.AddAttempt(frame, pinsdown);

                if (pinsdown == 10)
                {
                    pinsdown = Roll(10);
                } else pinsdown = Roll(10-pinsdown);
                
                p.FramesSet.AddAttempt(frame, pinsdown); // third attempt attempt
            }

            if ((frame == 9)
                && (p.FramesSet.Frames[9].Attempts[0] < 10)
                && (p.FramesSet.Frames[9].Attempts.Sum() == 10)
                && (p.FramesSet.Frames[9].Attempts.Count == 2)) // frame 10 spare
            {
                pinsdown = Roll(10);
                p.FramesSet.AddAttempt(frame, pinsdown);// add attempt
            }
        }

        int Roll(int pins)
        {
            int pdown = rnd.Next(pins+1 );
            return pdown;
        }
    }
}
