﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BowlingGame
{
    public class FrameSet
    {
        public struct Frame
        {
            public List<int> Attempts;
            public List<string> AttemptsString;
            public int FrameScore;
        };

        public FrameSet()
        {
            Frames = new Frame[10];
            for (int i = 0; i <= 9; i++)
            {
                Frames[i].Attempts = new List<int>();
                Frames[i].AttemptsString = new List<string>();
                Frames[i].FrameScore = 0;
            }
        }

        public Frame[] Frames { get; set; }
     
        public void CalculateFrameScore(int frame)
        {
           int score = 0;
           for (int frameindex = 0; frameindex <= frame; frameindex++)
            {
                Frame f = Frames[frameindex];

                score = f.Attempts.Sum();

                if (frameindex < 9)
                {
                    if (f.AttemptsString.Contains(" X"))
                    {
                        int sc = 0;

                        try
                        {
                            if (Frames[frameindex + 1].Attempts[0] == 10)
                            {
                                if (frameindex < 8)
                                    sc = Frames[frameindex + 1].Attempts[0] + Frames[frameindex + 2].Attempts[0];//take just attempt 1, if next frame is strike
                                if (frameindex == 8)
                                    sc = Frames[9].Attempts[0] + Frames[9].Attempts[1];
                            }
                            else
                                sc = Frames[frameindex + 1].Attempts[0] + Frames[frameindex + 1].Attempts[1];// take attempt 1 and 2 from the last frame
                            int temp_index = frameindex + 1;
                        }
                        catch (Exception)
                        {
                            sc = 0;
                        }
                        finally
                        {
                            score = score + sc;
                        }

                    }

                    if (f.AttemptsString.Contains(" \\"))
                    {
                        int sc = 0;
                        try
                        {
                            sc = Frames[frameindex + 1].Attempts[0];// get only 1 attempt value from the next frame
                        }
                        catch (Exception)
                        {
                            sc = 0;
                        }
                        finally
                        {
                            score = score + sc;
                        }
                    }
                }
                Frames[frameindex].FrameScore = score;
            }
                

           // Frames[frameindex].FrameScore = score;
         
        }

        public int TotalScore(int current_frame)
        {
            int score = 0;
            for (int i = 0; i <= current_frame; i++)
            {
                score += Frames[i].FrameScore;
            }
            return score;
        }

        public void AddAttempt(int frame_index, int pins_down)
        {
            Frame f = Frames[frame_index];

            f.Attempts.Add(pins_down);

            if (frame_index < 9)
            {
                if ((pins_down == 10) && (f.Attempts.Count == 1))
                {
                    f.AttemptsString.Add(" X");
                    f.AttemptsString.Add("  ");
                }
                else if ((f.Attempts.Count == 2) && (f.Attempts.Sum() == 10))
                {
                    f.AttemptsString.Add(" \\");
                }
                else if (pins_down > 0)
                { f.AttemptsString.Add(pins_down.ToString("0#")); }
                else f.AttemptsString.Add(" -");
            }


            if (frame_index == 9)
            {
                if (pins_down == 10)
                {
                    f.AttemptsString.Add(" X");
                }
                else if ((f.Attempts.Count == 2) && (f.Attempts[0] + f.Attempts[1] == 10)&&(f.Attempts[1]>0))
                {
                    f.AttemptsString.Add(" \\");
                }
                else if ((f.Attempts.Count == 3) && (f.Attempts[1] + f.Attempts[2] == 10) && (f.Attempts[0] + f.Attempts[1] < 10))
                {
                    f.AttemptsString.Add(" \\");
                }
                else f.AttemptsString.Add(pins_down.ToString("0#"));
            }

        }

        public override string ToString()
        {
            string retstring = "";
            string framenumber = "               ";
            string line = "               ";
            string attemptstrings = "Attempts:     |";
               string framescores = "Frame score:  |";
             string currentscores = "Total Score:  |";

            for (int f = 0; f < 10;f++)//attempts line
            {
                Frame fr = Frames[f];
                framenumber += "Fr. " + (f+1).ToString()+"   ";
                line += "========";
                
                if (fr.AttemptsString.Count > 0)
                {
                    attemptstrings += " ";
                    foreach (string att in fr.AttemptsString)
                    {
                        attemptstrings += " " + att;
                    }
                    attemptstrings += "|";
                }
                else attemptstrings += "       ";
            }

            int currscore = 0;
            for (int f = 0; f < 10; f++)//FrameScore line and Total score 
            {
                Frame fr = Frames[f];
                int framescore = fr.FrameScore;
                currscore = TotalScore(f);
                if (fr.AttemptsString.Count==0)
                {
                    framescores += "        ";
                    currentscores += "       ";
                }
                else
                {
                    if (fr.AttemptsString.Count == 3)
                    {
                        framescores += "        " + fr.FrameScore.ToString("0#") + "|";
                        currentscores += "       " + currscore.ToString("00#") + "|";
                        line += "===";
                    }
                    else
                    {
                        framescores += "     " + fr.FrameScore.ToString("0#") + "|";
                        currentscores += "    " + currscore.ToString("00#") + "|";
                    }
                }  
            }
            
            retstring = framenumber+"\n"
                            + line + "\n" 
                            + attemptstrings + "\n" 
                            + framescores + "\n" 
                            + currentscores + "\n" 
                            + line+" Total score:"+ currscore.ToString()+"\n";
            return retstring;
        }
    }

}
