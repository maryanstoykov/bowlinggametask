﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BowlingGame
{
    public class Player
    {
        public Player(string name)
        {
            Name = name;

            FramesSet = new FrameSet();

        }

        public FrameSet FramesSet { get; set; }
        //public int CurrentFrame { get; set; }
        public string Name { get; set; }

    }
}
