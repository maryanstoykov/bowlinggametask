﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BowlingGame
{
    class Program
    {
        static void Main(string[] args)
        {
            
          
            do
            {
                Game bg = new Game();
                bg.Start();
                Console.Write("**************** GAME OVER ****************\nPress \"N\" to exit..");
             } while (Console.ReadKey().Key != ConsoleKey.N);
             

            // tests
            /* 
            Player p = new Player("TEST PLAYER");

            p.FramesSet.AddAttempt(0, 10);
            //p.FramesSet.AddAttempt(0, 0);
            p.FramesSet.AddAttempt(1, 10);
            //p.FramesSet.AddAttempt(1, 0);
            p.FramesSet.AddAttempt(2, 10);
            //p.FramesSet.AddAttempt(2, 0);
            p.FramesSet.AddAttempt(3, 10);
            //p.FramesSet.AddAttempt(3, 0);
            p.FramesSet.AddAttempt(4, 10);
            //p.FramesSet.AddAttempt(4, 0);
            p.FramesSet.AddAttempt(5, 10);
            //p.FramesSet.AddAttempt(5, 0);
            p.FramesSet.AddAttempt(6, 10);
            //p.FramesSet.AddAttempt(6, 0);
            p.FramesSet.AddAttempt(7, 10);
            //p.FramesSet.AddAttempt(7, 0);
            p.FramesSet.AddAttempt(8, 10);
            //p.FramesSet.AddAttempt(8, 0);
            p.FramesSet.AddAttempt(9, 10);
            p.FramesSet.AddAttempt(9, 10);
            p.FramesSet.AddAttempt(9, 10);

            p.FramesSet.CalculateFrameScore(9);

            Console.WriteLine(p.FramesSet.ToString());
            Console.ReadLine();
            */

            /*
            List<Player> players = new List<Player>()
            {
                new Player("Player 1"),
                new Player("Player 2"),
                new Player("Player 3"),
                new Player("Player 4")
            };

            Game bg = new BowlingGame(players);
            bg.Start();
            */

        }
    }
}
